import java.util.ArrayList;
import java.util.List;

public class Graph {

    private List<V> vertices;
    private String hash;
    private int st = 0;
    private List<Graph> stGraph;

    public Graph(){}

    public Graph(int n, int m){

        vertices = new ArrayList<>();
        for(int i = 0; i < n; i++){
//            System.out.println("i: " + i);
            V v = new V(i);
            List<U> ulist = new ArrayList<>();
            for (int j = 0; j < m; j++){
//                System.out.print(" j: " + j);
                ulist.add(new U(j));
            }
            v.setU(ulist);
            vertices.add(v);
        }
    }

    public Graph(String hash){
        vertices = new ArrayList<>();
        int lenght = hash.length();
        int[] vIndex= new int[lenght/2];
        int[] uIndex= new int[lenght/2];
        int vIterator =0,uIterator =0;
//        System.out.println("hash lenght " + lenght);
        this.hash = hash;
        for (int i=0; i< lenght; i++){
            char c = hash.charAt(i);
            if (c == 'v'){
                vIndex[vIterator]=i+1;
                vIterator++;
            }
            else {
                if (c =='u'){
                uIndex[uIterator]=i+1;
                uIterator++;
                }
                else {
                 System.out.println("BAD STATE");
                }
            }
        }
        for(int i = 0; i < vIndex.length ; i++){
            List<U> ulist = new ArrayList<>();
            for (int j = 0; j < uIndex.length; j++){
                ulist.add(new U(j,uIndex[j]));
            }
            V v = new V(i,vIndex[i], ulist);
            vertices.add(v);
        }
    }

    public List<V> getVertices() {
        return vertices;
    }

    public void setVertices(List<V> vertices) {
        this.vertices = vertices;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getSt() {
        return st;
    }

    public void setSt(int st) {
        this.st = st;
    }

    public List<Graph> getStGraph() {
        return stGraph;
    }

    public void setStGraph(List<Graph> stGraph) {
        this.stGraph = stGraph;
    }
}
