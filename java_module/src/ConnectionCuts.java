import java.util.List;

public class ConnectionCuts {
    public void cutPotencialConnectionsBibartiteNew(int V_globalPositionIndex, int U_globalPostionIndex, Graph tnew, int n) {

        if (V_globalPositionIndex == U_globalPostionIndex) {
            return;
        }
        // a is v' index iterator in T areas
        for (int a = 0; a < tnew.getVertices().size(); a++) {
            int vv_globalIndex = tnew.getVertices().get(a).getGlobalPostionIndex();
            if (vv_globalIndex == V_globalPositionIndex) {
                continue;
            }
            // split on interval  v -> u  an u <- v, first division v>u
            if (V_globalPositionIndex > U_globalPostionIndex) {
                //  this means V > U and example is V vertex number 5 is goint to vertex U number 2
                // todo VV lower than U cannot got o U-V(V-U) interval
                if (vv_globalIndex < U_globalPostionIndex) {
                    for (int b = 0; b < n / 2; b++) {
                        int uu_globalIndex = tnew.getVertices().get(a).getU().get(b).getGlobalPositionIndex();
                        if (uu_globalIndex > U_globalPostionIndex && uu_globalIndex < V_globalPositionIndex) {
                            setBlock(a, b, tnew, V_globalPositionIndex, U_globalPostionIndex);
                        }
                    }
                }
                // todo VV between U-V(V-U) cannot go anywhere
                if (vv_globalIndex > U_globalPostionIndex && vv_globalIndex < V_globalPositionIndex) {
                    for (int b = 0; b < n / 2; b++) {
                        setBlock(a, b, tnew, V_globalPositionIndex, U_globalPostionIndex);
                    }
                }
                // todo VV bigger than U-V(V-U) cannot go V-U
                if (vv_globalIndex > V_globalPositionIndex) {
                    for (int b = 0; b < n / 2; b++) {
                        int uu_globalIndex = tnew.getVertices().get(a).getU().get(b).getGlobalPositionIndex();
                        if (uu_globalIndex > U_globalPostionIndex && uu_globalIndex < V_globalPositionIndex) {
                            setBlock(a, b, tnew, V_globalPositionIndex, U_globalPostionIndex);
                        }
                    }
                }
            } else {
                if (V_globalPositionIndex < U_globalPostionIndex) {
                    //  this means V < U and example is V vertex number 2 is goint to vertex U number 5
                    // todo VV lower than V cannot got o V - U interval
                    if (vv_globalIndex < V_globalPositionIndex) {
                        for (int b = 0; b < n / 2; b++) {
                            int uu_globalIndex = tnew.getVertices().get(a).getU().get(b).getGlobalPositionIndex();
                            if (uu_globalIndex < U_globalPostionIndex && uu_globalIndex > V_globalPositionIndex) {
                                setBlock(a, b, tnew, V_globalPositionIndex, U_globalPostionIndex);
                            }
                        }
                    }
                    // todo VV between V-U cannot go anywhere
                    if (vv_globalIndex < U_globalPostionIndex && vv_globalIndex > V_globalPositionIndex) {
                        for (int b = 0; b < n / 2; b++) {
                            setBlock(a, b, tnew, V_globalPositionIndex, U_globalPostionIndex);
                        }
                    }
                    // todo VV bigger than V-U cannot go V-U
                    if (vv_globalIndex > U_globalPostionIndex) {
                        for (int b = 0; b < n / 2; b++) {
                            int uu_globalIndex = tnew.getVertices().get(a).getU().get(b).getGlobalPositionIndex();
                            if (uu_globalIndex < U_globalPostionIndex && uu_globalIndex > V_globalPositionIndex) {
                                setBlock(a, b, tnew, V_globalPositionIndex, U_globalPostionIndex);
                            }
                        }
                    }
                } else {
                    System.out.println("FAILED STATE");
                }
            }
        }
    }


    private void setBlock(int a, int b, Graph tnew, int V_globalPositionIndex, int U_globalPostionIndex) {
        if (tnew.getVertices().get(a).getU().get(b).getState() == State.FREE) {
//            System.out.println("u index = " + U_globalPostionIndex +
//                    " v index " + V_globalPositionIndex +
//                    "v'<v outer case B " + b +
//                    " v' " + tnew.getVertices().get(a).getVertexNumber() +
//                    " u'" + tnew.getVertices().get(a).getU().get(b).getNumber()
//            );
            tnew.getVertices().get(a).getU().get(b).setState(State.BLOCK);
        }
    }













    public void cutPotencialConnections(int v_index, int u_index, List<V> tnew) {
        if (v_index == u_index) {
            return;
        }
        // a is v' index iterator in T areas
        for (int a = 0; a < tnew.size(); a++) {
            if (a == v_index) {
                continue;
            }
            if (a < v_index) {
                // outer case
                // do b dam u' prve po v t.j pre v = 3 tak u' = 4
                for (int b = v_index; b < u_index; b++) {
                    if (tnew.get(a).getU().get(b).getState() == State.FREE) {
//                        System.out.println("u index = "+u_index+" v index "+v_index+"v'<v outer case B " + b + " v' " + tnew.get(a).getVertexNumber() +" u'" + tnew.get(a).getU().get(b).getNumber());
                        tnew.get(a).getU().get(b).setState(State.BLOCK);
                    }
                }
            } else {
                if (a <= u_index) {
                    //inner
                    // nastav po zaciatok pre u' mensie ako v
                    for (int b = v_index - 1; b >= 0; b--) {
                        if (tnew.get(a).getU().get(b).getState() == State.FREE) {
//                            System.out.println("u index = "+u_index+" v index "+v_index+"v' > v inner case  v' <= u to start B " + b + " v' " + tnew.get(a).getVertexNumber() + " u'" + tnew.get(a).getU().get(b).getNumber());
                            tnew.get(a).getU().get(b).setState(State.BLOCK);
                        }
                    }
                    // nastav po koniec pre u' vacie ako u
                    for (int b = u_index + 1; b < tnew.get(a).getU().size(); b++) {
                        if (tnew.get(a).getU().get(b).getState() == State.FREE) {
//                            System.out.println("u index = "+u_index+" v index "+v_index+" v' > v inner case  v' <= u to end B "
//                                    + b + " v' " + tnew.get(a).getVertexNumber() + " u'" + tnew.get(a).getU().get(b).getNumber());
                            tnew.get(a).getU().get(b).setState(State.BLOCK);
                        }
                    }
                } else {
                    //outer
                    for (int b = u_index - 1; b >= v_index; b--) {
                        if (tnew.get(a).getU().get(b).getState() == State.FREE) {
//                            System.out.println("u index = "+u_index+" v index "+v_index+"v' > v outer case  v' > u to end B " + b + " v' " + tnew.get(a).getVertexNumber() + " u'" + tnew.get(a).getU().get(b).getNumber());
                            tnew.get(a).getU().get(b).setState(State.BLOCK);
                        }
                    }
                }
            }
//System.out.println("B " + b + " T->A " + tnew.get(a).getVertexNumber() + " u'" + tnew.get(a).getU().get(b).getNumber());
        }
    }

}
