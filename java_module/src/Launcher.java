import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Launcher {

    public static void main(String[] args) {

        GraphGenerator graphGenerator = new GraphGenerator();
        CountHandler countHandler = new CountHandler();
        BufferedWriter writer = null;
        File logFile = new File("results.txt");

        try {
            writer = new BufferedWriter(new FileWriter(logFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // MAIN CYCLE x= number of vertexes on one part of graph

        for (int x = 2; x <8 ; x++) {
            int n = 2*x;
            HashMap<String, Graph> indexCombination = new HashMap<>();
            int minimal = n;
            int[] global = new int[n];
            Graph resultGraph ;
            List<String> globalCombinationList = new ArrayList<>();
            List<List<String>> allCombinedCombinationsList = new ArrayList<>();

            /* global numbers used for vertex number association */
            for (int i = 0; i < n; i++) {
                global[i] = i + 1;
            }

            /* Create All vertex combinations */
            for (int i = 0; i < n; i++) {
                List<Integer> usedNumbers = new ArrayList<>();
                int number = i + 1;
                usedNumbers.add(number);
                //System.out.println("Start number " + (number));
                graphGenerator.recursive(global, usedNumbers, indexCombination);
            }

//            /* print all hashed combinations */
//            for (Graph value : indexCombination.values()) {
//                System.out.println(" Hash " + value.getHash());
////                System.out.print("V: ");
////                for (V v : value.getVertices()) {
////                    System.out.print(" | index " + v.getGlobalPostionIndex() + " vert num " + v.getVertexNumber());
////                }
////                System.out.println("");
//            }

            /* create all possible connections list */
            for (int a = 0; a < x; a++) {
                for (int b = 0; b < x; b++) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(a);
                    stringBuilder.append("-");
                    stringBuilder.append(b);
                    globalCombinationList.add(stringBuilder.toString());
                    System.out.print(" "+ stringBuilder.toString());
                }
            }

            /* create all possible combinations of connections */
            for(String s : globalCombinationList){
                List<String> usedList = new ArrayList<>();
                usedList.add(s);
                graphGenerator.recursiveConnCombinations(globalCombinationList,usedList,allCombinedCombinationsList); /// asi zle ze iterujem global combination list
                // todo recursive creation of combination list and then launch count handler without for cycles ?
            }

            resultGraph= new Graph();
            System.out.println("\nHash table vertexes combinations list size " + indexCombination.size());
            System.out.println("Connections combinations arraylist size " + allCombinedCombinationsList.size());
            System.out.println("Connections 1 combination arraylist size " + allCombinedCombinationsList.get(0).size());


            /* count all areas */
            for (Graph value : indexCombination.values()) {

                for (List<String> combination: allCombinedCombinationsList) {
                    List<Graph> st = new ArrayList<>();

                    int temp = countHandler.countPagesKbipartiteNewWithConnections(value, st, n, null, minimal, combination);
                    //OLD without connection combinations
                    // int temp = countHandler.countPagesKbipartiteNew(value, st, n, null, minimal);

                    if (temp < minimal) {
                        resultGraph = value;
                        resultGraph.setStGraph(st);
                        minimal = temp;
                        resultGraph.setSt(minimal);
                    }
                }
            }
            /* final print */
            countHandler.printResultGraph(resultGraph, x, writer);
        }

        try {
            // Close the writer regardless of what happens...
            if (writer != null) {
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("EXC " + e);
            e.printStackTrace();
        }
    }


    public void oldCode() {

        GraphGenerator graphGenerator = new GraphGenerator();
        List<V> mainV;
        List<V> partT;
        List<List<V>> st;
        BufferedWriter writer = null;
        File logFile = new File("results.txt");

        try {
            writer = new BufferedWriter(new FileWriter(logFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int x = 2; x < 5; x++) {
            mainV = new ArrayList<>();
            partT = new ArrayList<>();
            st = new ArrayList<>();
            mainV = graphGenerator.createCompleteBiparite(x, x, mainV);
            partT = graphGenerator.createCompleteBiparite(x, x, partT);
            st.add(partT);
            CountHandler countHandler = new CountHandler();
            countHandler.countAreas(mainV, st, x, x, writer);

        }
//        for(int x=3; x<6 ; x++) {
//            mainV = new ArrayList<>();
//            partT = new ArrayList<>();
//            st = new ArrayList<>();
//            mainV = graphGenerator.createCompleteGraph(x, mainV);
//            partT = graphGenerator.createCompleteGraph(x, partT);
//            st.add(partT);
//            CountHandler countHandler = new CountHandler();
//            countHandler.countAreasKcompl(mainV, st, x, writer);
//
//        }
        try {
            // Close the writer regardless of what happens...
            if (writer != null) {
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("EXC " + e);
            e.printStackTrace();
        }

    }
}


