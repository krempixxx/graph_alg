public class U {
    private int number;
    private State state=State.FREE;
    private int globalPositionIndex;

    public U(int number) {
        this.number = number;
    }
    public U(int number, State state) {
        this.number = number;
        this.state = state;
    }
    public U(int number, int globalPositionIndex) {
        this.number = number;
        this.globalPositionIndex = globalPositionIndex;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getGlobalPositionIndex() {
        return globalPositionIndex;
    }
    public void setGlobalPositionIndex(int globalPositionIndex) {
        this.globalPositionIndex = globalPositionIndex;
    }
}
