import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GraphGenerator {

    public List<V> createCompleteBiparite(int n, int m, List<V> vMain) {

        int n_squadred = n * 2;
        int m_squadred = m * 2;

        for (int i = 1; i < n_squadred; i += 2) {
//            System.out.println("i: " + i);
            V v = new V(i);
            List<U> ulist = new ArrayList<>();
            for (int j = 2; j <= m_squadred; j += 2) {
//                System.out.print(" j: " + j);
                ulist.add(new U(j));
            }
            v.setU(ulist);
            vMain.add(v);
        }
        return vMain;
    }

    public List<V> createCompleteGraph(int n, List<V> vMain) {
        for (int i = 1; i < n; i++) {
            V v = new V(i);
            List<U> ulist = new ArrayList<>();
            for (int j = 1; j <= n; j++) {
                if (i >= j) {
                    ulist.add(new U(j, State.BLOCK));
                } else {
                    ulist.add(new U(j));
                }
            }
            v.setU(ulist);
            vMain.add(v);
        }
        return vMain;
    }

    public void recursive(int[] gl, List<Integer> used, HashMap<String, Graph> combinationHashmap) {

        if (used.size() == gl.length) {
            String hash = hashVerteces(used, gl.length);
            if (!combinationHashmap.containsKey(hash)){
                Graph g = new Graph(hash);
                combinationHashmap.put(hash, g);
            }
            return;
        } else {
            for (int i = 0; i < gl.length; i++) {
                boolean flag = true;
                int tempNumber = 0;
                for (int j = 0; j <used.size(); j++) {
                    if (( i+1) == used.get(j)){
                        flag = false;
                    }
                }
                if (flag) {
                    tempNumber =gl[i];
                    List<Integer> newUsed = cloneAndAdd(used, tempNumber, new ArrayList<>());
                    recursive(gl, newUsed, combinationHashmap);
                }
            }
        }
    }

    public void recursiveConnCombinations(List<String> globalCombinationList, List<String> usedCombinations, List<List<String>> combList){

        if (globalCombinationList.size() == usedCombinations.size()){
            combList.add(usedCombinations);
//            System.out.print("Added: ");
//            for (String a: usedCombinations){
//                System.out.print(" "+ a);
//            }
//            System.out.println(" ");
            return;
        }
        for (String glComb: globalCombinationList){
            boolean flag = true;
            for (String usedComb: usedCombinations){
                if (usedComb.equals(glComb)){
                    flag = false;
                }
            }
            if (flag){
                List<String> newUsed = cloneStrCombAndAdd(usedCombinations,glComb, new ArrayList<>());
                recursiveConnCombinations(globalCombinationList, newUsed, combList);
            }
        }
    }

    public String hashVerteces(List<Integer> indexSetup, int n) {
            StringBuilder stringBuilder = new StringBuilder();
            for (Integer number : indexSetup){
                if (number <= n/2 ){
                    stringBuilder.append('v');
                }
                else {
                    stringBuilder.append('u');
                }
            }
            return stringBuilder.toString();
    }

    public List<String> cloneStrCombAndAdd(List<String> old, String newString, List<String> newList){
        for (String combination: old){
            newList.add(new String(combination));
        }
        newList.add(newString);
        return newList;
    }

    public List<Integer> cloneAndAdd(List<Integer> old, int newNumber, List<Integer> newList) {
        //newList.addAll(old);
        for (Integer index: old){
            newList.add(new Integer(index));
        }
        newList.add(newNumber);
        return newList;
    }
}
