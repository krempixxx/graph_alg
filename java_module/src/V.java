import java.util.List;

public class V {
    private int vertexNumber;
    private int globalPostionIndex;
    private List<U> u;

    public V(int vertexNumber) {
        this.vertexNumber = vertexNumber;
    }

    public V(int vertexNumber, int globalPostionIndex) {
        this.vertexNumber = vertexNumber;
        this.globalPostionIndex = globalPostionIndex;
    }
    public V(int vertexNumber, int globalPostionIndex, List<U> u) {
        this.vertexNumber = vertexNumber;
        this.globalPostionIndex = globalPostionIndex;
        this.u = u;
    }

    public int getVertexNumber() {
        return vertexNumber;
    }


    public List<U> getU() {
        return u;
    }

    public void setU(List<U> u) {
        this.u = u;
    }

    public int getGlobalPostionIndex() {
        return globalPostionIndex;
    }

    public void setGlobalPostionIndex(int globalPostionIndex) {
        this.globalPostionIndex = globalPostionIndex;
    }
}
