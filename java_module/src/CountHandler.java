import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CountHandler {


    public int countPagesKbipartiteNew(Graph mainV, List<Graph> st, int n, BufferedWriter writer, int minimal) {
        ConnectionCuts cuts = new ConnectionCuts();
        for (int i = 0; i < mainV.getVertices().size(); i++) {
            V v = mainV.getVertices().get(i);
            for (int j = 0; j < mainV.getVertices().get(i).getU().size(); j++) {
                U u = mainV.getVertices().get(i).getU().get(j);
                boolean tFlag = false;
                // scout 2D areas in 3D
                for (Graph t : st) {
                    // if v -> u   true === FREE
                    if (t.getVertices().get(i).getU().get(j).getState() == State.FREE) {
                        t.getVertices().get(i).getU().get(j).setState(State.CONN);
                        // set on all V between v (v'>v)-> u(u'>u) to false
                        tFlag = true;
                        cuts.cutPotencialConnectionsBibartiteNew(v.getGlobalPostionIndex(), u.getGlobalPositionIndex(), t, n);
                        break;
                    }
                }
                if (!tFlag) {
                    Graph tnew = new Graph(mainV.getHash());
                    tnew.getVertices().get(i).getU().get(j).setState(State.CONN);
                    cuts.cutPotencialConnectionsBibartiteNew(v.getGlobalPostionIndex(), u.getGlobalPositionIndex(), tnew, n);
                    st.add(tnew);
                    tFlag = false;
                }
                if (st.size() > minimal) {
                    //System.out.println("" + minimal);
                    return minimal;
                }
            }
        }
//        printTandN(st, n, writer);
        //printT(st, mainV.getHash());
        return st.size();
    }


    public int countPagesKbipartiteNewWithConnections(Graph mainV, List<Graph> st, int n, BufferedWriter writer, int minimal, List<String> combination) {
        ConnectionCuts cuts = new ConnectionCuts();

        for (String s : combination) {
            String[] strarr = s.split("-");
            int i = Integer.parseInt(strarr[0]);
            int j = Integer.parseInt(strarr[1]);
            V v = mainV.getVertices().get(i);
            U u = mainV.getVertices().get(i).getU().get(j);
            boolean tFlag = false;
            // scout 2D areas in 3D
            for (Graph t : st) {
                // if v -> u   true === FREE
                if (t.getVertices().get(i).getU().get(j).getState() == State.FREE) {
                    t.getVertices().get(i).getU().get(j).setState(State.CONN);
                    // set on all V between v (v'>v)-> u(u'>u) to false
                    tFlag = true;
                    cuts.cutPotencialConnectionsBibartiteNew(v.getGlobalPostionIndex(), u.getGlobalPositionIndex(), t, n);
                    break;
                }
            }
            if (!tFlag) {
                Graph tnew = new Graph(mainV.getHash());
                tnew.getVertices().get(i).getU().get(j).setState(State.CONN);
                cuts.cutPotencialConnectionsBibartiteNew(v.getGlobalPostionIndex(), u.getGlobalPositionIndex(), tnew, n);
                st.add(tnew);
                tFlag = false;
            }
            if (st.size() > minimal) {
                //System.out.println("Min: " + minimal);
                return minimal;
            }
        }
//        printTandN(st, n, writer);
        //printT(st, mainV.getHash());
        return st.size();
    }


    public void printResultGraph(Graph graph, int n, BufferedWriter bufferedWriter) {

        int tn = 2 * n;
        float res = (float) tn / 3;
        float result = res + 1;

        System.out.print("| n " + n);
        System.out.print(" | T count in ST for hash: " + graph.getSt() + " " + graph.getHash());
        System.out.print(" | T count in ST should be 2*n/3 +1 dolne ohranicenie :");
        System.out.printf(" | %.3f |", result);
        System.out.println("");


        printT(graph.getStGraph(), graph.getHash());
        printTFile(graph.getStGraph(), n,bufferedWriter);
    }

    public void printT(List<Graph> st, String hash) {
        int x = 1;
        //System.out.println("T count in ST for hash: " + st.size() + " " + hash);
        for (Graph t : st) {
            for (V v : t.getVertices()) {
                for (U u : v.getU()) {
                    System.out.println("T: " + x + "number V: " + v.getVertexNumber() +
                            " index v " + v.getGlobalPostionIndex() +
                            " number u: " + u.getNumber() +
                            " index u " + u.getGlobalPositionIndex() +
                            " " + u.getState());
                }
            }
            x++;
        }
    }

    public void printTandN(List<List<V>> st, int n, BufferedWriter writer) {
        Date date = new Date();
        //Pattern for showing milliseconds in the time "SSS"
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String stringDate = sdf.format(date);
        System.out.println("N " + n + " | STs " + st.size() + " || time: " + stringDate + " || " + System.currentTimeMillis());
        try {
            writer.write("N " + n + " | STs " + st.size() + " || time: " + stringDate + " || " + System.currentTimeMillis() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void printTFile(List<Graph> st, int n, BufferedWriter writer) {
        Date date = new Date();
        //Pattern for showing milliseconds in the time "SSS"
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String stringDate = sdf.format(date);

        try {
            writer.write("N " + n + " | STs " + st.size() + " || time: " + stringDate + " || " + System.currentTimeMillis() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Count areas only for complete biparite graph with V as 1,3,5 - odd numbers
     * and U as 2,4,6
     *
     * @param mainV
     * @param st
     */
    public void countAreas(List<V> mainV, List<List<V>> st, int n, int m, BufferedWriter writer) {
        GraphGenerator graphGenerator = new GraphGenerator();
        ConnectionCuts cuts = new ConnectionCuts();
        //printT(st);
        for (int i = 0; i < mainV.size(); i++) {
            for (int j = 0; j < mainV.get(i).getU().size(); j++) {
                U u = mainV.get(i).getU().get(j);
                boolean tFlag = false;
                // scout 2D areas in 3D
                for (List<V> t : st) {
                    // if v -> u is  true === FREE
                    if (t.get(i).getU().get(j).getState() == State.FREE) {
                        t.get(i).getU().get(j).setState(State.CONN);
//                        System.out.println("u index = "+j+" v index "+i + " Vertex V " +t.get(i).getVertexNumber() +" U " + t.get(i).getU().get(j).getNumber());
                        // set on all V between v (v'>v)-> u(u'>u) to false
                        // because they will cross
                        tFlag = true;
                        // iterate t area vertexes but +1 than actual
                        cuts.cutPotencialConnections(i, j, t);
                        break;
                    }
                }
                if (!tFlag) {
                    List<V> tnew = new ArrayList();
                    tnew = graphGenerator.createCompleteBiparite(n, m, tnew);
//                    System.out.println("u index = "+j+" v index "+i + " Vertex V " +tnew.get(i).getVertexNumber() +" U " + tnew.get(i).getU().get(j).getNumber());
                    tnew.get(i).getU().get(j).setState(State.CONN);
                    cuts.cutPotencialConnections(i, j, tnew);
                    st.add(tnew);
                    tFlag = false;
                }
            }
        }
        printTandN(st, n, writer);
        //printT(st);
    }


    public void countAreasKcompl(List<V> mainV, List<List<V>> st, int n, BufferedWriter writer) {
        GraphGenerator graphGenerator = new GraphGenerator();
        //printT(st);
        for (int i = 0; i < mainV.size(); i++) {
            for (int j = 0; j < mainV.get(i).getU().size(); j++) {
                U u = mainV.get(i).getU().get(j);


                boolean tFlag = false;
                // scout 2D areas in 3D
                for (List<V> t : st) {


                    // if v -> u is  true === FREE
                    if (t.get(i).getU().get(j).getState() == State.FREE) {
                        t.get(i).getU().get(j).setState(State.CONN);
//                        System.out.println("u index = "+j+" v index "+i + " Vertex V " +t.get(i).getVertexNumber() +" U " + t.get(i).getU().get(j).getNumber());
                        // set on all V between v (v'>v)-> u(u'>u) to false
                        // because they will cross
                        tFlag = true;
                        // iterate t area vertexes but +1 than actual


                        // 1 parameter is bad and call is not working
                        //  cutPotencialConnectionsBibartiteNew(i, j, t,1);
                        break;
                    }
                }
                if (!tFlag) {
                    List<V> tnew = new ArrayList();
                    tnew = graphGenerator.createCompleteGraph(n, tnew);
//                    System.out.println("u index = "+j+" v index "+i + " Vertex V " +tnew.get(i).getVertexNumber() +" U " + tnew.get(i).getU().get(j).getNumber());
                    tnew.get(i).getU().get(j).setState(State.CONN);
                    // 1 parameter is bad and call is not working
                    //cutPotencialConnectionsBibartiteNew(i, j, tnew, 1);
                    st.add(tnew);
                    tFlag = false;
                }
            }
        }
        printTandN(st, n, writer);
        //printT(st);
    }
}
